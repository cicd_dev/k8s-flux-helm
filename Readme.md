# https://keyvatech.com/2022/06/06/gitops-flux-helm/

export GITLABTOKEN=

flux bootstrap gitlab --owner=cicd_dev --repository=k8s-flux-helm --token-auth --path=./clusters/dev-cluster --components-extra=image-reflector-controller,image-automation-controller --private=false --personal=false

helm repo add --username cicd_dev --password $GITLABTOKEN k8s-flux-helm https://gitlab.com/api/v4/projects/42746586/packages/helm/stable


helm create webhooks

k create ns webhooks
helm upgrade --install webhooks . -n webhooks

1. Get the application URL by running these commands:
export POD_NAME=$(kubectl get pods --namespace webhooks -l "app.kubernetes.io/name=webhooks,app.kubernetes.io/instance=webhooks" -o jsonpath="{.items[0].metadata.name}")
export CONTAINER_PORT=$(kubectl get pod --namespace webhooks $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
echo "Visit http://127.0.0.1:8080 to use your application"
kubectl --namespace webhooks port-forward $POD_NAME 8080:$CONTAINER_PORT

helm package webhooks

helm plugin install https://github.com/chartmuseum/helm-push

helm cm-push webhooks-0.1.0.tgz k8s-flux-helm

flux create source helm webhooks --url=https://gitlab.com/api/v4/projects/42746586/packages/helm/stable --interval=5m --username=cicd_dev --password=$GITLABTOKEN --namespace demo

flux create hr webhookshr --chart=webhooks --source=HelmRepository/k8s-flux-helm --chart-version="0.1.0" --namespace demo --interval=10m
flux delete hr webhookshr

flux create hr webhooks --chart=webhooks --source=HelmRepository/webhooks --chart-version="0.1.0" --namespace demo --interval=5m

------------------------------------------------
multipass info kube

helm package webhooks-dev

helm cm-push webhooks-0.1.3.tgz k8s-flux-helm

flux create source helm webhooks --url=https://gitlab.com/api/v4/projects/42571463/packages/helm/development --interval=5m --username=cicd_dev --password=$GITLABTOKEN --namespace webhooks --export > clusters/dev-cluster/webhookssource.yaml

flux create hr webhooks --chart=webhooks --source=HelmRepository/webhooks --chart-version=">0.0.0" --namespace webhooks --interval=5m --export >./clusters/dev-cluster/webhookshr.yaml



flux delete hr webhooks -n webhooks
flux delete source helm webhooks -n webhooks

flux reconcile kustomization apps --with-source -n flux-system





